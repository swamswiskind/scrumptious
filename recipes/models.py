from django.db import models
from django.conf import settings

# Create your models here.

# represents recipes
class Recipe(models.Model):

    # recipe title
    title = models.CharField(max_length=200, help_text="Please enter the Recipe's title (max 200 characters): ")

    # recipe image
    image = models.URLField(help_text="Please provide the URL of the Recipe's featured image: ")

    # Description
    description = models.TextField(help_text="Please provide the Recipe's content")

    # Date created
    created_on = models.DateTimeField(("Created On"), auto_now_add=True)

    # Date updated
    updated_on = models.DateTimeField(("Updated On"), auto_now=True)

    # Create a many-to-one association, many recipes,
    # one author
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class Ingredient(models.Model):
    food_item = models.CharField(max_length=100)
    amount = models.CharField(max_length=100)
    recipe = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE)

    class Meta:
        ordering = ["food_item"]

class RecipeStep(models.Model):
    instruction = models.TextField()
    step_number = models.PositiveSmallIntegerField()
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)

    class Meta:
        ordering = ["step_number"]
