# Generated by Django 4.2 on 2023-04-07 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0005_rename_shop_list_ingredient_recipes'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='amount',
            field=models.CharField(default='1', max_length=200),
            preserve_default=False,
        ),
    ]
