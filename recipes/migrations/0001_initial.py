# Generated by Django 4.2 on 2023-04-05 21:33

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text="Please enter the Recipe's title (max 200 characters): ", max_length=200)),
                ('image', models.URLField(help_text="Please provide the URL of the Recipe's featured image: ")),
                ('description', models.TextField(help_text="Please provide the Recipe's content")),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='')),
            ],
        ),
    ]
