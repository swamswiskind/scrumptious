from django.contrib import admin
from recipes.models import Recipe, Ingredient, RecipeStep

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
        "description",
        "updated_on",
        "created_on",
        "author"
    )

@admin.register(Ingredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        "food_item",
        "amount",
        "id",
        "recipe",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "instruction",
        "step_number",
        "id",
        "recipe",
    )
